
$(document).ready(function() {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gBASE_URL_DRINK ="http://42.115.221.44:8080/devcamp-pizza365/drinks";
    const gGET_VOUCHER_BASE_URL = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail";
    const vCREATE_ORDER_BASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    /* Mảng userObjects là mảng chứa dữ liệu user. Từng phần tử là object
    * id: tự tăng lên 1. Ví dụ, hiện id lớn nhất là 6, thì khi thêm user mới, id sẽ là 7
    */
    // biến lưu loại pizza
    var gPizzaType = "";
    // biến lưu loại combo được chọn
    var gComboPizza = "";
    // biến lưu giá trị tên loại nước uống
    var gtenLoaiNuocUong = 0;
    // biến lưu giá trị phần trăm giảm giá
    var gPercentDiscount = 0;
    // biến xác định call voucher thành công hay thất bại
    var gResultCallVoucheAjax = false;
    // biến xác định call create order thành công hay thất bại
    var gResultCallCreateOrderAjax = false;
    // mảng lưu giá trị phản hồi khi gọi API lấy voucher
    var gResponseVoucherAjax = [];
    // mảng lưu giá trị phản hồi khi tạo đơn hàng
    var gReponseCreateOrder = [];
    // biến lưu trữ tất cả dữ liệu đơn hàng
    var gInforOrder = {
        kichCo: "",
        duongKinh: "", 
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: 0,
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    }
    // biến lưu dữ liệu loại pizza được chọn
    var gInfoPizzaType = {
        title: "",
        subTitle: "",
        description: ""
    }
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();
    // phần chọn comnbo, có các lựa chọn S, M, L--------------------------------------------------
    $("#btn-s").on("click", function() {
        console.log("Chọn combo S được click!");
        onBtnComboSClick();
    });
    $("#btn-m").on("click", function() {
        console.log("Chọn combo M được click!");
        onBtnComboMClick();
    });
    $("#btn-l").on("click", function() {
        console.log("Chọn combo L được click!");
        onBtnComboLClick();
    });
    // phần chọn loại pizza, có các lựa chọn seafood, hawaii, bacon -------------------------------
    $("#btn-seafood").on("click", function() {
        console.log("Chọn loại Seafood được click!");
        onBtnSeafoodTypeClick();
    });
    $("#btn-hawaii").on("click", function() {
        console.log("Chọn loại Hawaii được click!");
        onBtnSeaHawaiiTypeClick();
    });
    $("#btn-bacon").on("click", function() {
        console.log("Chọn loại Bacon được click!");
        onBtnSeaBaconTypeClick();
    });
    // phần các nút nhấn khác------------------------------------------------
    // nút gửi đơn
    $("#btn-gui-don-hang").on("click", function() {
        console.log("Nút gửi được click được click!");
        onBtnSendOrderClick();
    });
    // nút tạo đơn trên modal tạo đơn
    $(document).on("click", "#btn-tao-don", function() {
        console.log("Nút tạo đơn được click được click được click!");
        onBtnCreatedOrderClick();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // hàm xử lý khi trang load
    function onPageLoading(){
        "use strict";
        getDataFormAjaxToSelectDrink();
    }
    // hàm xử lý khi nút chọn combo S được click
    function onBtnComboSClick(){
        "use strict";
        gComboPizza = "S"
        //b1: thu thập dữ liệu
        getDataComboS();
        //b2: valid data (bỏ qua)
        //b3: xử lý nghiệp vụ thay đổi màu nút được chọn
        changeColorBtnCombo(gComboPizza);
        //b4: hiển thị data
        showDataComboToConsole();
    }
    // hàm xử lý khi nút chọn combo M được click
    function onBtnComboMClick(){
        "use strict";
        gComboPizza = "M"
        //b1: thu thập dữ liệu
        getDataComboM();
        //b2: valid data (bỏ qua)
        //b3: xử lý nghiệp vụ thay đổi màu nút được chọn
        changeColorBtnCombo(gComboPizza);
        //b4: hiển thị data
        showDataComboToConsole();
    }
    // hàm xử lý khi nút chọn combo M được click
    function onBtnComboLClick(){
        "use strict";
        gComboPizza = "L"
        //b1: thu thập dữ liệu
        getDataComboL();
        //b2: valid data (bỏ qua)
        //b3: xử lý nghiệp vụ thay đổi màu nút được chọn
        changeColorBtnCombo(gComboPizza);
        //b4: hiển thị data
        showDataComboToConsole();
    }
    // hàm xử lý khi nút chọn kiểu seafood được click
    function onBtnSeafoodTypeClick(){
        "use strict";
        gPizzaType = "Seafood";
        // b1: thu thập dữ liệu
        getDataSeafoodType();
        // b2: valid data (bỏ qua);
        // b3: xử lý nghiệp vụ thay đổi màu nút được chọn
        changeColorBtnPizzaType(gPizzaType);
        // b4: Hiển thị dữ liệu ra console
        showDataPizzaTypeToConsole();
    }
    // hàm xử lý khi nút chọn kiểu hawaii được click
    function onBtnSeaHawaiiTypeClick(){
        "use strict";
        gPizzaType = "Hawaii";
        // b1: thu thập dữ liệu
        getDataHawaiiType();
        // b2: valid data (bỏ qua);
        // b3: xử lý nghiệp vụ thay đổi màu nút được chọn
        changeColorBtnPizzaType(gPizzaType);
        // b4: Hiển thị dữ liệu ra console
        showDataPizzaTypeToConsole();
    }
    // hàm xử lý khi nút chọn kiểu bacon được click
    function onBtnSeaBaconTypeClick(){
        "use strict";
        gPizzaType = "Bacon";
        // b1: thu thập dữ liệu
        getDataBaconType();
        // b2: valid data (bỏ qua);
        // b3: xử lý nghiệp vụ thay đổi màu nút được chọn
        changeColorBtnPizzaType(gPizzaType);
        // b4: Hiển thị dữ liệu ra console
        showDataPizzaTypeToConsole();
    }
    // hàm xử lý khi gửi đơn hàng được click
    function onBtnSendOrderClick(){
        "use strict";
        // b1: thu thập dữ liệu
        getDataOrderInfor();
        // b2: valid dữ liệu
        // b2.1: valid tất cả dữ liệu trừ voucher id
        var vIsDataValidate = validateInforCustomer();
        // b2.2: valid dữ liệu cho voucher id
        var vValidateVoucher = checkVoucherbyVoucherId();
        // b3: call API
        if(vValidateVoucher && vIsDataValidate){
            // gọi API
            callAPIGetVoucher();
            console.log(gResponseVoucherAjax);
            //B3.2 xử lý phản hồi
            processResponseGetVoucher();
        }
        // hiển thị dữ liệu
        if(vIsDataValidate) {
            // B4: ghi dữ liệu vào vùng màu vàng
            showDataInforCustomer();
            processModalInforOrder();
            }
         
    }
    // hàm xử lý khi nút tạo đơn được click
    function onBtnCreatedOrderClick(){
        //b1: thu thập dữ liệu (bỏ qua)
        //b2: valid (bỏ qua)
        //b3: gọi API
        callApiCreateOrder();
        console.log(gReponseCreateOrder);
        //b4: xử lý phản hồi
        if(gResultCallCreateOrderAjax){
            processReponseCreateOrder();
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm lấy dữ liệu từ combo S vào biến global
    function getDataComboS(){
        "use strict";
        gInforOrder.kichCo = "S";
        gInforOrder.duongKinh = "20cm";
        gInforOrder.suon = 2;
        gInforOrder.salad = "200g";
        gInforOrder.soLuongNuoc = 2;
        gInforOrder.thanhTien = 150000;
    }
    // hàm lấy dữ liệu từ combo M vào biến global
    function getDataComboM(){
        "use strict";
        gInforOrder.kichCo = "M";
        gInforOrder.duongKinh = "25cm";
        gInforOrder.suon = 4;
        gInforOrder.salad = "300g";
        gInforOrder.soLuongNuoc = 3;
        gInforOrder.thanhTien = 200000;
    }
    // hàm lấy dữ liệu từ combo L vào biến global
    function getDataComboL(){
        "use strict";
        gInforOrder.kichCo = "L";
        gInforOrder.duongKinh = "30cm";
        gInforOrder.suon = 8;
        gInforOrder.salad = "500g";
        gInforOrder.soLuongNuoc = 4;
        gInforOrder.thanhTien = 250000;
    }
    // hàm đổi màu nút nhấn chọn combo
    function changeColorBtnCombo(paramKeyColor){
        "use strict";
        if(paramKeyColor === "S"){
            $("#btn-s").removeClass().addClass("bg-success btn btn-w50-h50");
            $("#btn-m").removeClass().addClass("bg-orange btn btn-w50-h50");
            $("#btn-l").removeClass().addClass("bg-orange btn btn-w50-h50");
        }
        if(paramKeyColor === "M"){
            $("#btn-s").removeClass().addClass("bg-orange btn btn-w50-h50");
            $("#btn-m").removeClass().addClass("bg-success btn btn-w50-h50");
            $("#btn-l").removeClass().addClass("bg-orange btn btn-w50-h50");
        }
        if(paramKeyColor === "L"){
            $("#btn-s").removeClass().addClass("bg-orange btn btn-w50-h50");
            $("#btn-m").removeClass().addClass("bg-orange btn btn-w50-h50");
            $("#btn-l").removeClass().addClass("bg-success btn btn-w50-h50");
        }
    }
    // hàm hiển thị dữ liệu ra console
    function showDataComboToConsole(){
        "use strict";
        console.log("Size: " + gInforOrder.kichCo);
        console.log("Đường kính: " + gInforOrder.duongKinh);
        console.log("Sườn nướng: " + gInforOrder.suon);
        console.log("Salad: " + gInforOrder.salad);
        console.log("Nước ngọt: " + gInforOrder.soLuongNuoc);
        console.log("Giá bán VND: " + gInforOrder.thanhTien);
    }
    // hàm lấy dữ liệu chọn seafood vào biến global
    function getDataSeafoodType(){
        "use strict";
        gInforOrder.loaiPizza = gPizzaType;
        gInfoPizzaType.title = "OCEAN MANIA";
        gInfoPizzaType.subTitle = "PIZZA HẢI SẢN XỐT MAYONNAISE";
        gInfoPizzaType.description = "Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây.";
    }
    function getDataHawaiiType(){
        "use strict";
        gInforOrder.loaiPizza = gPizzaType;
        gInfoPizzaType.title = "HAWAIIAN";
        gInfoPizzaType.subTitle = "PIZZA DĂM BÔNG DỨA KIỂU HAWAII";
        gInfoPizzaType.description = "Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.";
    }
    function getDataBaconType(){
        "use strict";
        gInforOrder.loaiPizza = gPizzaType;
        gInfoPizzaType.title = "CHEESY CHICKEN BACON";
        gInfoPizzaType.subTitle = "PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI";
        gInfoPizzaType.description = "Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.";
    }
    // hàm đổi màu nút nhấn chọn combo
    function changeColorBtnPizzaType(paramKeyColor){
        "use strict";
        if(paramKeyColor === "Seafood"){
            $("#btn-seafood").removeClass().addClass("btn bg-success w-100");
            $("#btn-hawaii").removeClass().addClass("btn bg-orange w-100");
            $("#btn-bacon").removeClass().addClass("btn bg-orange w-100");
        }
        if(paramKeyColor === "Hawaii"){
            $("#btn-seafood").removeClass().addClass("btn bg-orange w-100");
            $("#btn-hawaii").removeClass().addClass("btn bg-success w-100");
            $("#btn-bacon").removeClass().addClass("btn bg-orange w-100");
        }
        if(paramKeyColor === "Bacon"){
            $("#btn-seafood").removeClass().addClass("btn bg-orange w-100");
            $("#btn-hawaii").removeClass().addClass("btn bg-orange w-100");
            $("#btn-bacon").removeClass().addClass("btn bg-success w-100");
        }
    }
    // hàm show dữ liệu pizza type ra console
    function showDataPizzaTypeToConsole(){
        "use strict";
        console.log("Tiêu đề: " + gInfoPizzaType.title);
        console.log("Tiêu đề phụ: " + gInfoPizzaType.subTitle);
        console.log("Mô tả: " + gInfoPizzaType.description);
    }
    // hàm call API và đổ vào select drink
    function getDataFormAjaxToSelectDrink(){
        // b1: thu thập dữ liệu (bở qua)
        // b2: valid (bở qua)
        // b3: call API
        "use strict";
            // gọi api load dữ liệu users
            $.ajax({
            url: gBASE_URL_DRINK,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (objectDrink) {
                console.log(objectDrink);
                // b4: hiển thị dữ liệu đến select drink
                showToSelectDrink(objectDrink);
            }
            });
    }
    // hàm đổ dữ liệu đến select drink
    function showToSelectDrink(paramObjectDrink){
        "use strict";
        for(var bI = 0; bI < paramObjectDrink.length; bI ++){
            var vOption = $("<option>").val(paramObjectDrink[bI].maNuocUong).text(paramObjectDrink[bI].tenNuocUong);
            $("#select-drinks").append(vOption);
        }
    }
    // hàm lấy dữ liệu từ các trường input gửi đơn hàng
    function getDataOrderInfor(){
        "use strict";
        gInforOrder.hoTen = $("#inp-fullname").val().trim();
        gInforOrder.email = $("#inp-email").val().trim();
        gInforOrder.soDienThoai = $("#inp-dien-thoai").val().trim();
        gInforOrder.diaChi = $("#inp-dia-chi").val().trim();
        gInforOrder.idVourcher = $("#inp-voucher-id").val().trim();
        gInforOrder.loiNhan = $("#inp-message").val().trim();
        gInforOrder.idLoaiNuocUong = $("#select-drinks option:selected").val();
        gtenLoaiNuocUong = $("#select-drinks option:selected").text();
    }
    // hàm valid dữ liệu thông tin đơn hàng trừ voucher id
    function validateInforCustomer(){
        var vMailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(gComboPizza === ""){
            alert("Quý khách chưa chọn size pizza!");
            return false;
        }
        if(gPizzaType === ""){
            alert("Quý khách chưa chọn loại pizza!");
            return false;
        }
        if(gInforOrder.idLoaiNuocUong === ""){
            alert("Quý khách chưa chọn loại nước uống!");
            return false;
        }
        if(gInforOrder.hoTen === ""){
            alert("Họ tên phải được điền vào!");
            return false;
        }
        if(vMailFormat.test(gInforOrder.email) === false || gInforOrder.email === ""){
            alert("Email chưa điền hoặc sai định dạng");
            return false;
        }
        if(gInforOrder.soDienThoai === "" || isNaN(gInforOrder.soDienThoai) === true){
            alert("Trường điện thoại phải điền và phải là số!");
            return false;
        }
        if(gInforOrder.diaChi === ""){
            alert("Địa chỉ phải được điền vào!");
            return false;
        }
        return true;
    }
    // hàm check có bị rỗng hoặc không phải số hay không, nếu đúng thì không cần call API
    function checkVoucherbyVoucherId(){
        "use strict";
        if(gInforOrder.idVourcher === "" || isNaN(gInforOrder.idVourcher) === true){
            gInforOrder.idVourcher = "Không có";
            gPercentDiscount = 0;
            return false;
        }
        else{
            return true;
        }
    }
    // hàm gọi API lấy thông tin vocher
    function callAPIGetVoucher(){
        "use strict";
            // gọi api load dữ liệu users
            $.ajax({
            url: gGET_VOUCHER_BASE_URL + "/" + gInforOrder.idVourcher,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (objectVoucher) {
                console.log(objectVoucher);
                gResponseVoucherAjax = objectVoucher;
                gResultCallVoucheAjax = true;
                // b4: hiển thị dữ liệu đến select drink
                //showToSelectDrink(objectVoucher);
            },
            error: function (ajaxContext) {
                gResultCallVoucheAjax = false;
              }
            });
    }
    // hàm xử lý phản hồi voucher
    function processResponseGetVoucher(){
        "use strict";
        if(gResultCallVoucheAjax === true){
            gPercentDiscount = parseInt(gResponseVoucherAjax.phanTramGiamGia);
            gInforOrder.idVourcher = gResponseVoucherAjax.maVoucher;
        }
        else{
            gInforOrder.idVourcher = "Không có";
            gPercentDiscount = 0;
        }
    }
    // hàm hiển thị dữ liệu sau khi đã valid thành công
    function showDataInforCustomer(){
        "use strict";
        console.log("Valid dữ liệu thành công!");
        console.log("Size pizza: " + gComboPizza);
        console.log("Loại pizza: " + gPizzaType);
        console.log("Loại nước uống: " + gtenLoaiNuocUong);
        console.log("Họ tên: " + gInforOrder.hoTen);
        console.log("Email: " + gInforOrder.email);
        console.log("Số điện thoại: " + gInforOrder.soDienThoai);
        console.log("Địa chỉ: " + gInforOrder.diaChi);
        console.log("Mã giảm giá: " + gInforOrder.idVourcher);
        console.log("Phần trăm giảm giá: " + gPercentDiscount);
        console.log("Số điện thoại: " + gInforOrder.soDienThoai);
    }
    // hàm xử lý khi mở modal và truyền dữ liệu vào khi gửi được click
    function processModalInforOrder(){
        "use strict";
        var vPhaiThanhToan = gInforOrder.thanhTien - (gInforOrder.thanhTien * gPercentDiscount * 0.01);
        // gán dữ liệu lên modal
        $("#input-ho-ten").val(gInforOrder.hoTen);
        $("#input-so-dien-thoai").val(gInforOrder.soDienThoai);
        $("#input-dia-chi").val(gInforOrder.diaChi);
        $("#input-loi-nhan").val(gInforOrder.loiNhan);
        $("#input-ma-giam-gia").val(gInforOrder.idVourcher);
        var vHTML = "";
        vHTML += "<p class='form-control bg-light over-flow-info-order'>"
        vHTML += "Xác nhận: " + gInforOrder.hoTen + ", " + gInforOrder.soDienThoai + ", " + gInforOrder.diaChi + "<br>";
        vHTML += "Menu" + " " + gComboPizza + ", sườn nướng " + gInforOrder.suon + ", nước " + gInforOrder.soLuongNuoc + " ..." + "<br>";
        vHTML += "Loại pizza:" + " " + gPizzaType + ", Giá: " + gInforOrder.thanhTien + " vnd, Mã giảm giá: " + gInforOrder.idVourcher + "<br>";
        vHTML += "Phải thanh toán: " + vPhaiThanhToan + " vnd (giảm giá " + gPercentDiscount + "%)";
        vHTML += "</p>";
        $("#input-thong-tin-chi-tiet").html(vHTML);
        //mở modal lên
        $("#create-order-modal").modal("show");
    }
    // hàm gọi api tạo đơn hàng
    function callApiCreateOrder(){
        "use strict";
        gReponseCreateOrder = [];
        $.ajax({
          url: vCREATE_ORDER_BASE_URL,
          type: 'POST',
          async: false,
          contentType: 'application/json;charset=UTF-8', // added data type
          data : JSON.stringify(gInforOrder),
          success: function (newOrderObj) {
            console.log(Object.entries(newOrderObj));
            gResultCallCreateOrderAjax = true;
            gReponseCreateOrder = newOrderObj;
          },
          error: function (ajaxContext) {
              gResultCallCreateOrderAjax = false;
              $("#error-create-order-modal").modal("show");
          }
        });
    }
    // hàm xử lý dữ liệu nhận về từ ajax khi gọi API
    function processReponseCreateOrder(){
        "use strict";
        $("#create-order-modal").modal("hide");
        $("#success-create-order-modal").modal("show");
        $("#input-ma-don-hang-moi").val(gReponseCreateOrder.orderId);
        $("#inp-fullname").val("");
        $("#inp-email").val("");
        $("#inp-dien-thoai").val("");
        $("#inp-dia-chi").val("");
        $("#inp-voucher-id").val("");
        $("#inp-message").val("");
    }
//ngoặc kết thúc===============================================
});